﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Where_and_Sort_By_Descending_Example
{
    class Person
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }

        public override string ToString()
        {
            return $"First: {FirstName}, Last: {LastName}, Age: {Age}";
        }
    }

    class NameLengthComparer : IComparer<Person>
    {
        public int Compare(Person x, Person y)
        {
            var xLength = x.FirstName.Length;
            var yLength = y.FirstName.Length;

            if (xLength == yLength)
            {
                return x.Age.CompareTo(y.Age);
            }

            return x.FirstName.Length.CompareTo(y.FirstName.Length);
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            IList<Person> people = new List<Person>
            {
                new Person() {Age = 17, FirstName = "Lauryn", LastName="McFall"},
                new Person() {Age = 14, FirstName = "Nathaniel", LastName="McFall"},
                new Person() {Age = 11, FirstName = "Rachel", LastName="McFall"},
                new Person() {Age = 46, FirstName = "Leanne", LastName="McFall"},
                new Person() {Age = 46, FirstName = "Ryan", LastName="McFall"},
            };

            var olderPeople = people.Where(p => p.Age >= 17);
            Console.WriteLine("Older people: ");
            foreach (var person in olderPeople)
            {
                Console.WriteLine(person);
            }

            var sortedOlderPeople = olderPeople.OrderByDescending(p => p.FirstName);

            Console.WriteLine("\nOlder people, sorted by first name in descending order");
            foreach (var person in sortedOlderPeople)
            {
                Console.WriteLine(person);
            }
            

            var sortedByNameLengthOlderPeople = olderPeople.OrderByDescending(p => p, new NameLengthComparer());
            Console.WriteLine("\nOlder people, sorted by length of name, longest first (ties show oldest person first)");
            foreach (var person in sortedByNameLengthOlderPeople)
            {
                Console.WriteLine(person);
            }

            Console.WriteLine("Press enter to terminate");
            Console.ReadLine();
        }
    }
}
